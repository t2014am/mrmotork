<?php

require_once __DIR__ . '/../../vendor/autoload.php';

const CONFIG_VIEWS_DIR = __DIR__ . '/../views';
const CONFIG_DATA_DIR = __DIR__ . '/../../data';
const CONFIG_ASSETS_DIR = __DIR__ . '/../../web/assets';

// New constants
const CONFIG_MODELS_DIR = __DIR__ . '/../models';
const CONFIG_DB_DIR = __DIR__ . '/../../db';
const SQLITE_DB_FILE = CONFIG_DB_DIR . '/motork_dev_test.db';