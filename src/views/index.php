<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/multimodel.style.css" type="text/css" media="all">
    <link rel="stylesheet" href="/assets/more.style.css" type="text/css" media="all">
    <title>MK Dealer</title>
</head>
<body>
<h1>MK Cars.</h1>

<?php
include CONFIG_VIEWS_DIR . "/components/carslisting.php";
?>

</body>
</html>