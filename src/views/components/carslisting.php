<section class="multimodel__slider">
    <div class="grid">
        <?php foreach ($cars as $car) { ?>
            <div class="grid__item u-12/12--medium u-6/12--large u-4/12--large-x">

                <article class="card">
                    <a href="/detail/<?php echo $car->attrs->carId; ?>">

                        <figure class="card__picture">
                            <div class="card__image">
                                <img src="<?php echo $car->attrs->img; ?>">
                            </div>
                        </figure>
                        <footer class="card__info">
                            <span class="make u-text--center"><?php echo $car->attrs->make; ?></span>
                            <span class="model u-text--center"><?php echo $car->attrs->model; ?></span>
                            <p class="u-text--center">Car ID: <?php echo $car->attrs->carId; ?></p>
                        </footer>
                    </a>

                </article>

            </div>
        <?php } ?>
    </div>
</section>
