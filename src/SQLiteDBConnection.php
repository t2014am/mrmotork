<?php

class SQLiteDBConnection extends SQLite3
{


    /**
     * SQLiteDBConnection constructor.
     */
    public function __construct()
    {
    }

    /**
     * Self-explanatory createTableIfNotExist function.
     */
    public function createTableIfNotExist()
    {
        try {
            $db = new PDO('sqlite:' . SQLITE_DB_FILE);

            // Create Table "forms" into Database if not exists
            // We do not need to create a rowID, it is automatically created for us in SQLite by culumn name rowId
            $query = "CREATE TABLE IF NOT EXISTS forms (NAME TEXT, LASTNAME TEXT, EMAIL TEXT, PHONE TEXT, CAP TEXT, PRIVACY TEXT, CARID INT )";
            $db->exec($query);

            $db = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Self-explanatory insertData function.
     */
    public function insertData($formData)
    {
        try {
            //make db connection
            $db = new PDO('sqlite:' . SQLITE_DB_FILE);

            $insertData = "INSERT INTO forms VALUES ('$formData->name','$formData->surname','$formData->email','$formData->phone', '$formData->zip', '$formData->privacy', '$formData->carId')";
            $db->exec($insertData);

            //close db connection
            $db = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

