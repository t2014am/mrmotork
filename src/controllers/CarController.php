<?php

namespace Motork\controllers;

include(__DIR__ . '/../models/Model.php');
//include(CONFIG_MODELS_DIR . '/model.php');

use Motork\src\models\Model;

class CarController
{
    /**
     * Index Action
     *
     * This should contain the list of cars.
     */
    public function getIndex()
    {
        $model = new Model();
        $cars = $model->getAllCars();
        include CONFIG_VIEWS_DIR . '/index.php';
    }

    /**
     * Detail Action
     *
     * This should contain the car's detail and the form.
     */
    public function getDetail()
    {
        $id = explode("/", $_SERVER["REQUEST_URI"])[2];
        $model = new Model();
        $car = $model->getCarById($id);

        $cars = $this->getRelatedCars($car);
        $carsTitle = "Recommended Cars";

        include __DIR__ . '/../views/detail.php';
//        include CONFIG_VIEWS_DIR . '/detail.php';
    }

    /**
     * @return CarController
     */
    public static function create()
    {
        // this if statement is necessary to avoid duplicate resubmission
        if ($_POST) {

            // create an object with all the required fields
            $formData = new \stdClass();

            $formData->name = ($_POST['name']);
            $formData->surname = ($_POST['surname']);
            $formData->email = ($_POST['email']);
            $formData->phone = ($_POST['phone']);
            $formData->zip = ($_POST['zip']);
            $formData->privacy = ($_POST['privacy']);
            $formData->carId = ($_POST['carId']);

            $model = new Model();
            $model->create($formData);

            // Redirect to this page
            header("Location: " . $_SERVER['REQUEST_URI']);
            exit();
        }

        return new self();
    }

    /**
     * Get Related Cars
     *
     * This method coud be used to get related cars in comparison to the one selected. It lists all the cars that have at least one tag in common!
     * It could have been better done in Model instead of here if the data came out of a database, but we're using an api!
     */
    public function getRelatedCars($car)
    {
        $model = new Model();

        $relatedCarsSimilarInternalSpace = $model->getRelatedCars($car->tags->{'Internal space'});
        $relatedCarsSimilarSegment = $model->getRelatedCars($car->tags->Segment);
        $relatedCarsSimilarFuelType = $model->getRelatedCars($car->tags->{'Fuel type'});
        $relatedCarsSimilarLook = $model->getRelatedCars($car->tags->Look);
        $relatedCarsSimilarPrice = $model->getRelatedCars($car->tags->Price);

        $relatedCars = array_merge($relatedCarsSimilarInternalSpace, $relatedCarsSimilarSegment,
            $relatedCarsSimilarFuelType, $relatedCarsSimilarLook, $relatedCarsSimilarPrice);

        //To remove duplicates in an array of objects, the following is the best way to go
        return array_unique($relatedCars, SORT_REGULAR);
    }
}