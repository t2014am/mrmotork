<?php

namespace Motork\src\models;

//we need to include and use the created SQLiteDBConnection php file.
include(__DIR__ . '/../SQLiteDBConnection.php');

use SQLiteDBConnection;

// this is the model that communicates with the database
class Model
{
    function getAllCars()
    {
        $cars = json_decode(file_get_contents('http://localhost:8889/api.php/search'));
        $cars = $cars->data;

        return $cars;
    }

    function getCarById($id)
    {
        $car = json_decode(file_get_contents('http://localhost:8889/api.php/detail/' . $id));
        $car = $car->data;

        return $car;
    }

    function create($formData)
    {
        // we first connect with the db, if doesn't exist we create it
        $db = new SQLiteDBConnection();

        // then we check if the table exists, if not, we create it
        $db->createTableIfNotExist();

        //then we insert the data
        $db->insertData($formData);
    }

    /**
     * Get related cars, based on various values!
     */
    function getRelatedCars($filterTerm)
    {
        $cars = json_decode(file_get_contents('http://localhost:8889/api.php/search'));
        $cars = $cars->data;

        $carsFiltered = [];

        // this counter will be used to limit the results per related car!
        // I think this is a good way to recommend cars that have something in common.
        // If we want to show more, we can increase the control below!
        $counter = 0;
        foreach ($cars as $car) {
            foreach ($car->tags as $key => $value) {

                if ($value == $filterTerm && ($counter < 3)) {
                    $counter++;
                    array_push($carsFiltered, $car);
                }
            }
        }

        return $carsFiltered;
    }
}