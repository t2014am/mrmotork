<?php
/**
 * Created by PhpStorm.
 * User: t2010
 * Date: 08-06-18
 * Time: 17:15
 */

namespace Motork\src\models;

include(__DIR__ . '/../../src/models/Model.php');

use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{

    public function testGetAllCarsNotEmptyAndContains616()
    {
        $model = new Model();
        $actual = $model->getAllCars();
        $this->assertNotEmpty($actual);
        $this->assertContains("616", json_encode($actual));
        $this->assertCount(52, $actual);
        $this->assertGreaterThan(50, count($actual));

    }

    public function testGetCarByIdNotEmptyAndContains616()
    {
        $model = new Model();
        $actual = $model->getCarById(616);
        $this->assertNotEmpty($actual);
        $this->assertContains("616", json_encode($actual));
        $this->assertObjectHasAttribute('tags', $actual);
    }

    public function testCreate()
    {

    }

    public function testGetRelatedCarsNotEmptyAndContains616()
    {
        $model = new Model();
        $filterTerm = "1% percentile";
        $actual = $model->getRelatedCars($filterTerm);
        $this->assertNotEmpty($actual);
        $this->assertContains("616", json_encode($actual));
        $this->assertCount(1, $actual);
        $this->assertGreaterThan(0, count($actual));
        $this->assertNotNull($actual);


    }
}
