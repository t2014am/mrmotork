<?php

use Motork\controllers\CarController;

require_once __DIR__ . '/../src/lib/bootstrap.php';

$controller = CarController::create();

$urlParts = parse_url($_SERVER['REQUEST_URI']);
if (preg_match('#^/detail/([^/]+)$#', $urlParts['path'], $matches)) {
    $controller->getDetail();
} elseif (isset($_POST['submit'])) {
    //this else if is required to see if a post request has been made by id "submit" if yes, then go to the create
    // function of the mentioned controller!
    $controller->create();
} else {
    $controller->getIndex();
}

